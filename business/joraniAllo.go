package business

import (
	"encoding/json"
	"findmyspot/model"
	"findmyspot/utils"
	"github.com/gocolly/colly"
	"log"
)

func FindMissingDev() []string {
	utils.CheckMinimumEnvVariable()
	joraniData := model.JoraniModel{}
	var returnMIssingDay []string
	c := colly.NewCollector(
		colly.UserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36"),
	)
	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("Cookie", "jorani_session=zjarZbZa6OucV9k%2FDybfZxHtO1RYSFyJ1EXJvB8yE%2FkqCQmHJdQtRZohXcD%2Bp09qBO8EKWdILveB%2B5jD5I6rAS4f8hWYpLuPUsvPqWJYBv63hZjPRNd%2F0QgQWj%2Fol9oimbOacmRjcfUt5Pl0D%2B6Y1Tfd2avmYWFAk%2BWGStL8O%2BSxy4vMe8%2F81LWmBXDOuDDfBd8Vj%2FO3JfWsqMIbArI6yTxbWcGYxFDGWteX5nJuUogspDzYwym%2BZtjEoD9dztVh99wLG337VjEpNuFBYXab17zZVMKCseuDaxQlFe2u0Wswn3GEd%2Fr3OOna4qsrE596J9Uz%2F14J0egCzDdfsCYm2H4fsMNtpBAReDIOuNMCHTnPphpW6gcnuQCmgjRr4LSVuRifApIS%2FCN6rTLLy4LSS%2BhjvnPvW6qL%2FsRfDekLiTgh%2Fb7LmBsSUUGfm2w9Hq9CFPOaJai6YIGdxyN4yCqpF9tpClAO94piEheDIArrLwnP4BZuBEAr1D2bEoRmfGOBj8Ac9Mp1QP5GEuYSFVQWKJIBrhZDzvMwLe%2BJBMEPEZ%2FSjkfE3DZF%2B3jLZq5BMIgcRWLU%2B1DgaVGqhanmpaHLL91h1F0zulZKiPI4gmkgR7tqHXzU8j9I5jsXo0oD14t8jNAAoNB6ouvltRl5pkqEzv%2FdRfSWRd2FRaDxa5VV3qek%2F0%2BFJoltRtoXCQFqfiEYihV0i%2F%2B380xdpd8nwITrm%2BoHIwI1mWuWTQmkp8LDoJa7CTJfZicAZOhdrZWKPS%2FmL3SvIetkRSdUsZwZqvJewXBKPZdmsClPs34D2oxFha8v9e8W08wAkh3jLNVNiEDOML85njdNZ5AHwq9iE0qE3TNGuFvJhYcpNP7RrkHr%2BPIvfJZH518%2FBoYwCMNh%2BdGaTgIHDEX6iy0eqFzCXrjK%2FJl3lZ3z%2F7r2bZQwRca3amY%3D")
		r.Headers.Set("Upgrade-Insecure-Requests", "1")
	})
	c.OnScraped(func(r *colly.Response) {
		log.Println(r.StatusCode)
		//log.Println(string(r.Body))
		json.Unmarshal(r.Body, &joraniData)
		returnMIssingDay = PrintAvailableParkingSpot(&joraniData)
	})
	// start scraping
	err := c.Visit("http://jorani.hevea.lan/leaves/organization/2?children=true&start=2019-12-30&end=2020-02-10&_=1578066227915")
	if err != nil {
		log.Fatal(err)
	}
	if returnMIssingDay == nil {
		log.Println("No dev")
		return nil
	}
	log.Println(returnMIssingDay)
	return returnMIssingDay
}

