package business

import (
	"errors"
	"findmyspot/model"
	"findmyspot/utils"
	"net/http"
	"os"
	"strings"
	"time"
)

func CheckingJoraniData(jorani *model.JoraniModel) error {
	if len(*jorani) == 0 {
		return errors.New("cannot read jorani data")
	}
	return nil
}

func PrintAvailableParkingSpot(jorani *model.JoraniModel) []string {
	missingDay := model.JoraniModel{}
	missingDayPlusone := model.JoraniModel{}

	parking := utils.ReturnParking()
	var sendTotelegram []string
	var returnMIssingDay []string

	CheckingJoraniData(jorani)
	yearDParking := time.Now().YearDay()

	for _, val := range *jorani {

		yearSDJorani, _ := time.Parse("2006-01-02T15:04:05", val.Start)
		yearEDJorani, _ := time.Parse("2006-01-02T15:04:05", val.End)

		if yearDParking >= yearSDJorani.YearDay() && yearDParking <= yearEDJorani.YearDay() {
			missingDay = append(missingDay, val)
		}

		if yearDParking+1 >= yearSDJorani.YearDay() && yearDParking+1 <= yearEDJorani.YearDay() && (yearSDJorani.Hour() < 12 && yearEDJorani.Hour() >= 14) {
			if val.Color != "#f89406" {
				missingDayPlusone = append(missingDayPlusone, val)
			}
		}
	}

	for _, val := range missingDay {
		if len(parking[val.Title].Name) > 0 {
			sendTotelegram = append(sendTotelegram, "today : "+parking[val.Title].Place+" "+parking[val.Title].Name+"%0A")
			//fmt.Println("today : ", parking[val.Title])
			returnMIssingDay = append (returnMIssingDay, parking[val.Title].Name)
		}
	}
/** Utilisable pour le jour suivant
	for _, val := range missingDayPlusone {
		if len(parking[val.Title].Name) > 0 {
			sendTotelegram = append(sendTotelegram, "tmrw : "+parking[val.Title].Place+" "+parking[val.Title].Name+"%0A")
			fmt.Println("tmrw  : ", parking[val.Title])
		}
	}
*/
	if os.Getenv("TELEGRAM_BOT") != "" && os.Getenv("TELEGRAM_CHATID") != "" {
		SendTelegramNotif(&sendTotelegram)
	}

	return returnMIssingDay
}

func SendTelegramNotif (sendTotelegram *[]string){
	url := "https://api.telegram.org/"+os.Getenv("TELEGRAM_BOT")+"/sendMessage?chat_id="+os.Getenv("TELEGRAM_CHATID")+"&text="+strings.Join(*sendTotelegram, " ")
	_, err := http.Get(url)

	if err != nil {
		panic(err)
	}

}
