package business

import (
	"findmyspot/model"
	"testing"
)

func Test_CheckingJoraniData(t *testing.T) {
	Expected := "cannot read jorani data"
	joraniData := model.JoraniModel{}

	actual := CheckingJoraniData(&joraniData)

	if actual.Error() != Expected {
		t.Errorf("Error actual = %v, and Expected = %v.", actual, Expected)
	}
}
