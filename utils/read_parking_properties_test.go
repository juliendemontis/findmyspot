package utils

import (
	"testing"
)

func Test_ReadParkingProperties(t *testing.T) {
	ret := ReturnParking();
	if len(ret) != 28 {
		t.Error(
			"Parking value must be size of", 28,
		)
	}
}
