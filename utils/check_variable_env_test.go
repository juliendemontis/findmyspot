package utils

import (
	"testing"
	"os"
	"io/ioutil"
	"os/exec"
	"strings"
)

func Test_CheckMinimumEnvVariableNoParkingProperties (t *testing.T) {

	if os.Getenv("BE_CRASHER") == "1" {
		os.Setenv("PARKING_PROPERTIES","")
		CheckMinimumEnvVariable()
	}


	cmd := exec.Command(os.Args[0], "-test.run=Test_CheckMinimumEnvVariableNoParkingProperties")
	cmd.Env = append(os.Environ(), "BE_CRASHER=1")
	stdout, _ := cmd.StderrPipe()
	if err := cmd.Start(); err != nil {
		t.Fatal(err)
	}

	// Check that the log fatal message is what we expected
	gotBytes, _ := ioutil.ReadAll(stdout)
	got := string(gotBytes)
	expected := "Please define PARKING_PROPERTIES env variable"
	if !strings.HasSuffix(got[:len(got)-1], expected) {
		t.Fatalf("Unexpected log message. Got %s but should contain %s", got[:len(got)-1], expected)
	}

	// Check that the program exited
	err := cmd.Wait()
	if e, ok := err.(*exec.ExitError); !ok || e.Success() {
		t.Fatalf("Process ran with err %v, want exit status 1", err)
	}

}

func Test_CheckMinimumEnvVariableNoJoraniCookie (t *testing.T) {

	if os.Getenv("BE_CRASHER") == "1" {
		os.Setenv("JORANI_COOKIE","")
		CheckMinimumEnvVariable()
	}


	cmd := exec.Command(os.Args[0], "-test.run=Test_CheckMinimumEnvVariableNoJoraniCookie")
	cmd.Env = append(os.Environ(), "BE_CRASHER=1")
	stdout, _ := cmd.StderrPipe()
	if err := cmd.Start(); err != nil {
		t.Fatal(err)
	}

	// Check that the log fatal message is what we expected
	gotBytes, _ := ioutil.ReadAll(stdout)
	got := string(gotBytes)
	expected := "Please define JORANI_COOKIE env variable"
	if !strings.HasSuffix(got[:len(got)-1], expected) {
		t.Fatalf("Unexpected log message. Got %s but should contain %s", got[:len(got)-1], expected)
	}

	// Check that the program exited
	err := cmd.Wait()
	if e, ok := err.(*exec.ExitError); !ok || e.Success() {
		t.Fatalf("Process ran with err %v, want exit status 1", err)
	}

}
