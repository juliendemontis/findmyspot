package utils

import (
	"log"
	"strings"
	"findmyspot/model"
	"bufio"
	"os"
)

func ReturnParking () map[string]model.Place{


	file, err := os.Open(os.Getenv("PARKING_PROPERTIES"))
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	mapNameParkingSpot := make(map[string]model.Place)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		ret := strings.Split(scanner.Text(),"-")
		mapNameParkingSpot[strings.TrimLeft(ret[1]," ")] =  model.Place{Name: ret[1], Place : ret[0]}
	}

	return mapNameParkingSpot
}


