# findMySpot - Trouver ma place de parking en parsant Jorani

## Setup

* Définir une variable environnement : PARKING_PROPERTIES
  * Contenant le lien vers le fichier parking.properties
* Mettre son cookie Jorani dans la variable d'environnement : JORANI_COOKIE
  * Le cookie ne changeant pas, je n'ai pas implémenter l'authentification.
* Pour recevoir une alerte telegram il est nécessaire de renseigner en variable d'environnement:
  * le bot_id : TELEGRAM_BOT
  * le chat_id : TELEGRAM_CHATID

-------------------------------------------------------------------
(!) Etre soit à Reservit, soit en VPN sur le réseau reservit
