package model


type JoraniModel []struct {
	ID            string `json:"id"`
	Title         string `json:"title"`
	Start         string `json:"start"`
	Color         string `json:"color"`
	AllDay        bool   `json:"allDay"`
	End           string `json:"end"`
}

type Place struct {
	Place string `json:"utils"`
	Name string `json:"name"`
}